import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  myVar1:string;
  myVar2:string;
  myVar3:string;
  constructor() { 

  }

  ngOnInit() {
   this.myVar1='./assets/images/about/ab_logo.png';
   this.myVar2='./assets/images/about/csea_white.png';
   this.myVar3='./assets/images/about/CEG_white1.png';
  }

}
