import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { FormGroup, FormControl } from '@angular/forms';
import { MessageService } from '../message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  logForm = new FormGroup({
    cpassword : new FormControl(''),
    password: new FormControl(''),
    npassword: new FormControl('')
  })
  enabled : string

  loggedIn() {
    var cookies = {};
    var cookieString = document.cookie;
            var ca = cookieString.split(";");
            for(var i=0;i<ca.length;i++)
            {
                var cookie = ca[i];
                var keyval = cookie.split("=");
                var key = keyval[0];
                var value = keyval[1];
                cookies[key] = value;
            }
            if(cookies["user-token"])
                return true;
            return false;
  }

    constructor(private profileservice: ProfileService, private messageservice : MessageService, private router : Router) { 
  }

  ngOnInit(): void {
    this.messageservice.clear();
    this.enabled = "no-loader";
  }

  onSubmitLog() {
    var formvalues = this.logForm.value;
    if(formvalues["password"] == "")
      this.messageservice.add("Enter current password");
    else if(formvalues["cpassword"] == "")
      this.messageservice.add("Enter confirm password");
    else if(formvalues["npassword"] == "")
      this.messageservice.add("Enter new password");
    else if(formvalues["npassword"] != formvalues["cpassword"])
      this.messageservice.add("Password and confirm password does not match");
    else
    {
      var cookies = {};
            var cookieString = document.cookie;
            var ca = cookieString.split(";");
            for(var i=0;i<ca.length;i++)
            {
                var cookie = ca[i];
                var keyval = cookie.split("=");
                var key = keyval[0];
                var value = keyval[1];
                cookies[key] = value;
            }
            formvalues["token"] = cookies["user-token"];
            delete formvalues["cpassword"];
            this.enabled = "loader";
            $(".formContent").css("display","none");
      this.profileservice.chpass(formvalues).subscribe(resp => {
          this.logForm.reset();
          $(".formContent").css("display","block");
          this.enabled = "no-loader";
          if(resp["result-code"] == "200")
          {
            this.messageservice.add(resp["message"]);
          }
          else if(resp["result-code"] == "301")
          {
            document.cookie = "user-token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
          }
          else
          {
            this.messageservice.add(resp["message"]);
          }
      })
    }
  }

}
