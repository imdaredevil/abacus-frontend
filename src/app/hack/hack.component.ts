import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hack',
  templateUrl: './hack.component.html',
  styleUrls: ['./hack.component.css']
})
export class HackComponent implements OnInit {

  events_name="";
  events_desc : string ="";
  events_rules : string ="";
  events_contact : string ="";

  constructor() { }

  ngOnInit(): void {
    var active_link="#link-desch";
    var active_tab="#descriptionh";
    this.events_name="Chronus Hackathon";
    this.events_desc="";
    this.events_rules="";
    this.events_contact="";


    $('.hack').on('click',function(){
      
      $(".Complete-desc-bodyh").animate({height:'100%',width:'100%',left:'0%',top:'0%'});
      $('.close-icon').show();
    });

    $('.close-icon').on('click',function(){
      $(".Complete-desc-bodyh").animate({height:'0%',width:'0%',left:'50%',top:'50%'});
      $('.close-icon').hide();
    });
     $('#link-desch').on('click',function(){
        $(active_link).removeClass("link-active");
        $(active_tab).removeClass("content-active");
        $('#link-desch').addClass("link-active");
        $("#descriptionh").addClass("content-active");
        active_link="#link-desch";
        active_tab="#descriptionh";
     });
     $('#link-rulesh').on('click',function(){
      $(active_link).removeClass("link-active");
      $(active_tab).removeClass("content-active");
      $('#link-rulesh').addClass("link-active");
      $("#rulesh").addClass("content-active");
      active_link="#link-rulesh";
      active_tab="#rulesh";
     });
      $('#link-contacth').on('click',function(){
        $(active_link).removeClass("link-active");
        $(active_tab).removeClass("content-active");
        $('#link-contacth').addClass("link-active");
        $("#contacth").addClass("content-active");
        active_link="#link-contacth";
        active_tab="#contacth";
     });
  
  }
  hackreg() {
    window.open("https://forms.gle/ygZHEApmXQhfuvEu6","_blank")
  }
}
