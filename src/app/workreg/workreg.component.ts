import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ProfileService }  from '../profile.service';
import { MessageService } from '../message.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-workreg',
  templateUrl: './workreg.component.html',
  styleUrls: ['./workreg.component.css']
})

export class WorkregComponent implements OnInit {



  logForm = new FormGroup({
    referralCode: new FormControl('')
  })
  enabled : string

  loggedIn() {
    var cookies = {};
    var cookieString = document.cookie;
            var ca = cookieString.split(";");
            for(var i=0;i<ca.length;i++)
            {
                var cookie = ca[i];
                var keyval = cookie.split("=");
                var key = keyval[0];
                var value = keyval[1];
                cookies[key] = value;
            }
            if(cookies["user-token"])
                return true;
            return false;
  }

    constructor(private profileservice: ProfileService, private messageservice : MessageService,private route: ActivatedRoute) { 
  }

  ngOnInit(): void {
    this.messageservice.clear();
    this.enabled = "no-loader";
  }

  onSubmitLog() {
    var formvalues = this.logForm.value;
      var cookies = {};
      if(formvalues["referralCode"] == "")
        formvalues["referralCode"] = "none";
            var cookieString = document.cookie;
            var ca = cookieString.split(";");
            for(var i=0;i<ca.length;i++)
            {
                var cookie = ca[i];
                var keyval = cookie.split("=");
                var key = keyval[0];
                var value = keyval[1];
                cookies[key] = value;
            }
            formvalues["token"] = cookies["user-token"];
            if(formvalues["referralCode"] == cookies[" user-code"])
            {
                this.messageservice.add("You cannot refer yourself");
                return;
            }
            formvalues["workshopName"] = this.route.snapshot.paramMap.get("name");
            formvalues["workshopId"] = parseInt(this.route.snapshot.paramMap.get("id")) + 1;
            this.enabled = "loader";
            $(".formContent").css("display","none");
      this.profileservice.register(formvalues).subscribe(resp => {
          this.logForm.reset();
          $(".formContent").css("display","block");
          this.enabled = "no-loader";
          if(resp["result-code"] == "200")
          {
              window.open(resp["redirect_url"]);
          }
          else
          {
            this.messageservice.add(resp["message"]);
          }
      });
  }


}
