import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkregComponent } from './workreg.component';

describe('WorkregComponent', () => {
  let component: WorkregComponent;
  let fixture: ComponentFixture<WorkregComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkregComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkregComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
