import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

private backendUrl = "https://abacus.org.in/api"/* "http://0.0.0.0:8000"*/;
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-type' : 'application/json'})
  };

  signup (saccount: any): Observable<any> {
    var regurl = this.backendUrl + "/signup/"; 
    const formd = new FormData();
    for(const key in saccount)
      formd.append(key,saccount[key]);
    return this.http.post<any>(regurl,formd);
    }
  

  login (laccount: any): Observable<any> {
    var logurl = this.backendUrl + "/login/"; 
    const formd = new FormData();
    for(const key in laccount)
      formd.append(key,laccount[key]);
    return this.http.post<any>(logurl,formd);
  }


  register (workshop: any): Observable<any> {
    var regurl = this.backendUrl + "/workshopRegister/";
    const formd = new FormData();
    formd.append("userToken",workshop["token"]);
    formd.append("workshopId",workshop["workshopId"]);
    formd.append("referralCode",workshop["referralCode"]);
    formd.append("workshopName",workshop["workshopName"]);
    return this.http.post<any>(regurl, formd);
  }
  
  logout (loact: any): Observable<any> {
    var logurl = this.backendUrl + "/logout/"; 
    return this.http.post<any>(this.backendUrl, loact , this.httpOptions);
  }

  chpass(chpas: any): Observable<any> {
    var churl = this.backendUrl + "/changePassword/";
    const formd = new FormData();
    formd.append("token",chpas["token"]);
    formd.append("currentpassword",chpas["password"]);
    formd.append("newpassword",chpas["npassword"]);
    return this.http.post<any>(churl,formd);
  }

  paymentconfirm(reqid : any, status: any, id: any) : Observable<any> {
    var paurl = this.backendUrl + "/paymentConfirmation/";
    const formd = new FormData();
    formd.append("payment_id", id);
    formd.append("payment_request_id", reqid);
    formd.append("payment_status", status);
    return this.http.post<any>(paurl,formd);
  }

  getpayments(token: any) : Observable<any> {
    var getpayurl = this.backendUrl + "/getPayments/";
    const formd = new FormData();
    formd.append("userToken",token);
    return this.http.post<any>(getpayurl,formd);
  }

  isloggedIn(): boolean {
      var cookies = {};
      var cookieString = document.cookie;
      var ca = cookieString.split(";");
      for(var i=0;i<ca.length;i++)
      {
          var cookie = ca[i];
          var keyval = cookie.split("=");
          var key = keyval[0];
          var value = keyval[1];
          cookies[key] = value;
      }
      // console.log(cookies);
     // console.log(cookies[" user-token"]);
      if(" user-token" in cookies)
          return true;
      else return false;
  };
  
  forpass(email : string): Observable<any> {
    var fpurl = this.backendUrl + "/forgotPassword/";
    const formd = new FormData();
    formd.append("email",email);
      return this.http.post(fpurl,formd);
  }

  constructor(private http : HttpClient) {

   }
}
