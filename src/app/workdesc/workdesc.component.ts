import { Component, OnInit } from '@angular/core';
import { workshops_list } from '../workshops/workshops_list'

@Component({
  selector: 'app-workdesc',
  templateUrl: './workdesc.component.html',
  styleUrls: ['./workdesc.component.css']
})
export class WorkdescComponent implements OnInit {

  image_name:string;
  events_name=workshops_list[1].name;
  events_desc=workshops_list[1].detail_desc;
  events_contact=workshops_list[1].detail_desc;
  reglink: any;

  constructor() { 
  }

  ngOnInit() { 
   
  var active_link="#link-desc";
  var active_tab="#description";
  let ang = this;
  $('.know').on('click',function(){
    var id_str=$(this).attr('id');
    var id = parseInt(id_str[1]);
    ang.events_name=workshops_list[id].name;
    ang.events_desc=workshops_list[id].detail_desc;
    ang.events_contact=workshops_list[id].contact;
    ang.image_name = workshops_list[id].company_logo;
    ang.reglink = "/workreg" + "/" + id + "/" + ang.events_name + "/";
    if(id == 0 || id == 2 || id == 3)
    {
        $(".regdiv").css("display","none");
    }
    else
    {
      $(".regdiv").css("display","block");
    }
    if(window.innerHeight < window.innerWidth)
    $(".Complete-desc-body").animate({height:'80%',width:'80%',left:'10%',top:'10%'});
    else
    $(".Complete-desc-body").animate({height:'100%',width:'100%',left:'0%',top:'0%'});
    $('.close-icon').show();
    $('.menu-trigger').hide();
  });

 
  $('.close-icon').on('click',function(){
    $(".Complete-desc-body").animate({height:'0%',width:'0%',left:'50%',top:'50%'});
    $('.close-icon').hide();
    $('.menu-trigger').show();
  });
   $('#link-desc').on('click',function(){
      $(active_link).removeClass("link-active");
      $(active_tab).removeClass("content-active");
      $('#link-desc').addClass("link-active");
      $("#description").addClass("content-active");
      active_link="#link-desc";
      active_tab="#description";
   });
    $('#link-contact').on('click',function(){
      $(active_link).removeClass("link-active");
      $(active_tab).removeClass("content-active");
      $('#link-contact').addClass("link-active");
      $("#contact").addClass("content-active");
      active_link="#link-contact";
      active_tab="#contact";
    });
  }


}
