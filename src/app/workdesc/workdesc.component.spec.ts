import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkdescComponent } from './workdesc.component';

describe('WorkdescComponent', () => {
  let component: WorkdescComponent;
  let fixture: ComponentFixture<WorkdescComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkdescComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkdescComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
