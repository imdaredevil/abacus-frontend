import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileService } from '../profile.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-payment-status',
  templateUrl: './payment-status.component.html',
  styleUrls: ['./payment-status.component.css']
})
export class PaymentStatusComponent implements OnInit {

  constructor(private route : ActivatedRoute, private profileservice: ProfileService,public messageservice: MessageService) { }

  ngOnInit(): void {
    var reqid=this.route.snapshot.queryParamMap.get("payment_request_id");
    var status=this.route.snapshot.queryParamMap.get("payment_status");
    var id=this.route.snapshot.queryParamMap.get("payment_id");
    
    this.profileservice.paymentconfirm(reqid,status,id).subscribe(resp => {
      this.messageservice.add(resp["message"]);
      window.open("/");
    })
    
  }

}
