import { Component, OnInit, Input } from '@angular/core';
import { ProfileService } from '../profile.service'
declare const mytest: any;


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  
  title = 'app';
  loggedin : any;
  @Input() navBarColor : string;

  constructor(public profileservice : ProfileService) 
  {

  }

  loggedIn() {
    var cookies = {};
    var cookieString = document.cookie;
            var ca = cookieString.split(";");
            for(var i=0;i<ca.length;i++)
            {
                var cookie = ca[i];
                var keyval = cookie.split("=");
                var key = keyval[0];
                var value = keyval[1];
                cookies[key] = value;
            }
            if(cookies["user-token"])
                return true;
            return false;
  }

  ngOnInit() {
  }
}
