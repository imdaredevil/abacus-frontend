import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public messageservice : MessageService ) { }

  ngOnInit() {
    this.messageservice.add("An all night Hackathon from chronus.. <a href=\"/events\">Know more</a>")
    // values to keep track of the number of letters typed, which quote to use. etc. Don't change these values.
  }

}

