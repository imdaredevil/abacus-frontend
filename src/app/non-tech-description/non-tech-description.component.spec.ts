import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonTechDescriptionComponent } from './non-tech-description.component';

describe('NonTechDescriptionComponent', () => {
  let component: NonTechDescriptionComponent;
  let fixture: ComponentFixture<NonTechDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonTechDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonTechDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
