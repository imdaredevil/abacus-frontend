import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { events_list } from '../non-tech-events/non_tech_list';

@Component({
  selector: 'app-non-tech-description',
  templateUrl: './non-tech-description.component.html',
  styleUrls: ['./non-tech-description.component.css']
})
export class NonTechDescriptionComponent implements OnInit {

  image_name:string;
  events_name=events_list[1].name;
  events_desc : string =events_list[1].detail_desc;
  events_rules : string =events_list[1].rules;
  events_contact : string =events_list[1].contact;
  constructor() { 
    this.image_name="./assets/images/events/code.png";
   /* this.events_desc=events_list[this.id].short_desc;
    this.events_rules=events_list[this.id].name;
    this.events_contact=events_list[this.id].detail_desc;*/
  }

  ngOnInit() { 
    // Get the element with id="defaultOpen" and click on it
   // document.getElementById("defaultOpen").click();
  var active_link="#link-desc";
  var active_tab="#description";
  let ang = this;
  $('.know').on('click',function(){
    var id_str=$(this).attr('id');
    var id=parseInt(id_str[1]);
    ang.events_name=events_list[id].name;
    ang.events_desc=events_list[id].detail_desc;
    ang.events_rules=events_list[id].rules;
    ang.events_contact=events_list[id].contact;
    $(".Complete-desc-body").animate({height:'100%',width:'100%',left:'0%',top:'0%'});
    $('.close-icon').show();
  });
  /*$('#know_more1').on('click',function(){
    $(".Complete-desc-body").animate({height:'100%',width:'100%',left:'0%',top:'0%'});
    $('.close-icon').show();
  });*/
  $('.close-icon').on('click',function(){
    $(".Complete-desc-body").animate({height:'0%',width:'0%',left:'50%',top:'50%'});
    $('.close-icon').hide();
  });
   $('#link-desc').on('click',function(){
      $(active_link).removeClass("link-active");
      $(active_tab).removeClass("content-active");
      $('#link-desc').addClass("link-active");
      $("#description").addClass("content-active");
      active_link="#link-desc";
      active_tab="#description";
   });
   $('#link-rules').on('click',function(){
    $(active_link).removeClass("link-active");
    $(active_tab).removeClass("content-active");
    $('#link-rules').addClass("link-active");
    $("#rules").addClass("content-active");
    active_link="#link-rules";
    active_tab="#rules";
   });
    $('#link-contact').on('click',function(){
      $(active_link).removeClass("link-active");
      $(active_tab).removeClass("content-active");
      $('#link-contact').addClass("link-active");
      $("#contact").addClass("content-active");
      active_link="#link-contact";
      active_tab="#contact";
   });

}
}