import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonTechEventsComponent } from './non-tech-events.component';

describe('NonTechEventsComponent', () => {
  let component: NonTechEventsComponent;
  let fixture: ComponentFixture<NonTechEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonTechEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonTechEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
