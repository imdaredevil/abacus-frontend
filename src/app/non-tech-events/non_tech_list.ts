export const events_list = [
    {
        date: "6",
        month: "Mar",
        name:"Alacatraz",
        short_desc:"In every problem, there is a concealed solution, locking itself underneath, unlock, persue, find and ...",
        detail_desc: "In every problem, there is a concealed solution, locking itself underneath, unlock, persue, find and solve. Does the word “puzzle” excites you? Then this is the place for you. Come and join to compete with your fellow enigmatologist and win. Even the hardest puzzle have a solution."+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: R2</b><br></center>"+
        "<center><b>Time: 1:30pm to 4:30pm</b></center>",
        image: "./assets/images/events/code.png",
        rules: "<ul><li>Maximum of 3 per team.</li>"+
        "<li>No lone rangers will be allowed.</li>"+
        "<li>Arguments and any other form of cheating will lead to disqualification.</li>",
        contact:"<b>&nbsp;Sanjay</b> : +91 9442529435<br/><b>&nbsp;Deepak</b> : +91 9791909176 <br />"
    },
    {
        date: "6",
        month: "Mar",
        name:"Gamindrome",
        short_desc:"Non technical event for all the pros and noobs out there(COD MW, Rainbow six siege, NFSMW 2005) ... ",
        detail_desc:"Non technical event for all the pros and noobs out there !!<br/>"+ 
        "<ol><li><b>COD MW(Call of Duty 4: Modern Warfare -2007)</b><br/>"+
            "&nbsp;&nbsp;LAN event with TEAMS OF 4(no other form). Knife match toss with Team Deathmatch mains. Losing teams are KNOCKED OUT.Last standing 2 teams will compete for 1st & 2nd.</li>"+
        "<li><b>Rainbow six siege(Operation will be updated)</b><br/>"+
            "&nbsp;&nbsp;LAN event with TEAMS OF 5(no other form).Standard format. Losing teams are KNOCKED OUT.Last standing 2 teams will compete for 1st & 2nd.</li>"+
        "<li><b>NFSMW 2005</b></li>"+
            "&nbsp;&nbsp;Individual TIME TRIAL event(same track,choice of cars). Best record will be the winner.</li></ol>"+
            "<center><b>Click on this "+
            "<a href='https://docs.google.com/forms/d/e/1FAIpQLSeDFb3rkLlzFnGIZ2fksTTFaPz_QrFuOvUWvGeQMRaPEOqneQ/viewform'>link</a>"+
            " for Online Registration</b></center>"+
            "<center><br><b>Date: March 6</b></center>"+
            "<center><b>Venue: Second floor lab</b><br></center>"+
            "<center><b>Time: 9:30am to 12:30pm</b></center>",
        image: "./assets/images/events/icon3.png",
        rules: "<ul><li>Every team has to register to be on the event.</li>"+
        "<li>Late comers will be disqualified.</li>"+
        "<li>K/D will break a tie(COD).</li>"+
        "<li>Participants can bring their own peripherals.</li>"+
        "<li>Game format may change.</li><ul>",
        contact:"<b>&nbsp;Rishi </b> : +91 8903718747<br/>"+"<b>&nbsp;Bharat </b> : +91 8903774846"
    },
    {
        date: "6",
        month: "Mar",
        name:"Adzap",
        short_desc:"Adzap is set to bring out the Creativity, Time Management, Marketing Skills of the innovative ...",
        detail_desc:"Adzap is set to bring out the Creativity, Time Management, Marketing Skills of the innovative minds and to shape their team spirit and co-ordination. <br / > <center><b> “You dream it, we’ll promote it ” </b> <center> "+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: R4</b><br></center>"+
        "<center><b>Time: 1:30pm to 4:30pm</b></center>",
        image: "./assets/images/events/icon3.png",
        rules: "<ul><li>A team of 3 to 4 members can participate.</li>"+
        "<li>The event will be conducted in 2 rounds.</li>"+
        "<li>Participants will not be allowed to bring any props during the event.</li>"+
        "<li>Spontaneity, humour and creativity will be much appreciated.</li>"+
        "<li>Judges decision is final.</li>",
        contact:"<b>&nbsp;Kowsalya V</b> : +91 8098759076<br/><b>&nbsp;Subashini S</b> : +91 9976611264<br/>"
    },
    {
        date: "6",
        month: "Mar",
        name:"Mr. Detective 007",
        short_desc:"Calling all Super Sleuths out there. Get ready for a fun filled ride with riddles, puzzles ...",
        detail_desc:"Calling all Super Sleuths out there. Get ready for a fun filled ride with riddles, puzzles and maybe even… a murder mystery! So friends, put on your detective hats and bring out the magnifying glasses because the game is afoot! <br / > <center><b> “YOU SEE, BUT DO YOU OBSERVE?” </b> <center> "+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: R1</b><br></center>"+
        "<center><b>Time: 9:00am to 12:30pm</b></center>",
        image: "./assets/images/events/icon3.png",
        rules: "<ul><li>Maximum 2 per team.</li>"+
        "<li>A detective shouldn’t be a criminal!</li>"+
        "<li>The decision of the judges will be final.</li>",
        contact:"<b>&nbsp;Ashik</b> : +91 8668180171<br/><b>&nbsp;Prarthna</b> : +91 9677065410<br/> <b>&nbsp;Tanya</b> : +91 9840287183<br/><b>&nbsp;Tarani S</b> : +91 9489772673<br/>"
    },
    {
        date: "6",
        month: "Mar",
        name:"TREASURE HUNT",
        short_desc:"Time to explore CEG, Clues are just hidden. Full of fun. Get the move, hunt, chase, run and ...",
        detail_desc:"Time to explore CEG, Clues are just hidden. Full of fun. Get the move, hunt, chase, run and there you go, Victory is yours. <br / > <center><b> “Clues all around, knock knock!” </b> <center> ",
        image: "./assets/images/events/icon3.png",
        rules: "<ul><li>Team of three can participate</li>"+
        "<li>There will be two rounds</li>"+
        "<li>Time limit 45 mins</li>"+
        "<li>Judges decision is final</li>",
        contact:"<b>&nbsp;Kavin pragadesh</b> : +91 9445290070<br/><b>&nbsp;Kailash</b> : +91 9676959716<br/> <b>&nbsp;Priyadharshini</b> : +91 8248458697<br/> <b>&nbsp;Vaishnavi V</b> : +91 8681008183<br/>"
    },
    {
        date: "6",
        month: "Mar",
        name:"Kohliwood Rhapsody",
        short_desc:"Come to explore the  mashup of Kollywood and cricket in a single event. This is a cordial ...",
        detail_desc:"Alright! Alright! Alright! <br />"  +
        "What comes to your mind when you hear the word ‘THALA’ .If it’s DHONI you are in.If it’s  AJITH, you are also in. <br />" +
       "Come to explore the  mashup of Kollywood and cricket in a single event. <br /> "+
      "This is a cordial invite to all the Kollywood fans with the cricket craze. <br /> "+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: Turing hall</b><br></center>"+
        "<center><b>Time: 9:30pm to 12:30pm</b></center>",
        image: "./assets/images/events/icon3.png",
        rules: "<ul><li>A team of 2  or 3 members can participate</li>"+
        "<li>The event will be conducted in two rounds</li>"+
        "<li>Quiz masters decision is final</li>",
        contact:"<b>&nbsp;Gokkul</b> : +91 7418767287<br/><b>&nbsp;Saairam</b> : +91 8438536899<br/> <b>&nbsp;Abishiek</b> : +91 7904170134<br/><b>&nbsp;Deepak Raj</b> : +91 9080752316<br/> "
    },
    {
        date: "7",
        month: "Mar",
        name:"Dream Scribbler",
        short_desc:"To all the minds with detective and innovative ideas out there! A platform is set for exhibiting ...",
        detail_desc:"To all the minds with detective and innovative ideas out there! A platform is set for exhibiting your frames in  mind through pen, paper and laptop <br / > <center><b> “Dream it ,Design it” </b> <center> "+
        "<center><br><b>Date: March 7</b></center>"+
        "<center><b>Venue: R2</b><br></center>"+
        "<center><b>Time: 9:30am to 12:00pm</b></center>",
        image: "./assets/images/events/icon3.png",
        rules: "<ul><li>A team can have minimum of one member and maximum of two members</li>"+
        "<li>A participant cannot be a member of more than one team.</li>"+
        "<li>The participants must bring their laptops with pre-installed designing software</li>" +
        "<li>Decisions made by administrators will be final</li>",
        contact:"<b>&nbsp;Rohini</b> : +91 9751510095<br/><b>&nbsp;Kanika</b> : +91 8300295535<br/> <b>&nbsp;Manoj</b> : +91 9384393449<br/> <b>&nbsp;Nagarjun V</b> : +91 8220859980<br/>"
    },
    {
        date: "7",
        month: "Mar",
        name:"Houseful",
        short_desc:"We have picked house with crazy events. So put your shoes on and drop by for a gratifying ...",
        detail_desc:"We have picked house with crazy events. So put your shoes on and drop by for a gratifying experience"+
        "<center><br><b>Date: March 7</b></center>"+
        "<center><b>Venue: R1</b><br></center>"+
        "<center><b>Time: 9:00am to 12:00pm</b></center>",
        image: "./assets/images/events/icon3.png",
        rules: "<ul><li>Maximum of two per team</li>"+
        "<li>Judges decision is final</li>",
        contact:"<b>&nbsp;Pothi kannan</b> : +91 9500702083<br/><b>&nbsp;Siddharth</b> : +91 9626930040<br/> <b>&nbsp;Vignesh</b> : +91 9962891922<br/> "
    }
];
