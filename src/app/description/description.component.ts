import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';
import { events_list } from '../events/events_list';
@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent implements OnInit {
  image_name:string;
  events_name=events_list[1].name;
  events_desc : string =events_list[1].detail_desc;
  events_rules : string =events_list[1].rules;
  events_contact : string =events_list[1].contact;
  id: number;
  constructor() { 
    this.image_name="./assets/images/events/code.png";
   /* this.events_desc=events_list[this.id].short_desc;
    this.events_rules=events_list[this.id].name;
    this.events_contact=events_list[this.id].detail_desc;*/
  }

  ngOnInit() { 
    // Get the element with id="defaultOpen" and click on it
   // document.getElementById("defaultOpen").click();
  var active_link="#link-desc";
  var active_tab="#description";
  let ang = this;
  $('.know').on('click',function(){
    var id_str=$(this).attr('id');
    var id;
    if(id_str[1]=='A'){
      id=10;
    }
    else if(id_str[1]=='B'){
      id=11;
    }
    else if(id_str == "v12")
    {
      id = 12;
    }
    else{
      id=parseInt(id_str[1]);
    }
    ang.events_name=events_list[id].name;
    ang.events_desc=events_list[id].detail_desc;
    ang.events_rules=events_list[id].rules;
    ang.events_contact=events_list[id].contact;
    ang.id  = id;
    $(".Complete-desc-body").animate({height:'100%',width:'100%',left:'0%',top:'0%'});
    $('.close-icon').show();
  });
  /*$('#know_more1').on('click',function(){
    $(".Complete-desc-body").animate({height:'100%',width:'100%',left:'0%',top:'0%'});
    $('.close-icon').show();
  });*/
  $('.close-icon').on('click',function(){
    $(".Complete-desc-body").animate({height:'0%',width:'0%',left:'50%',top:'50%'});
    $('.close-icon').hide();
  });
   $('#link-desc').on('click',function(){
      $(active_link).removeClass("link-active");
      $(active_tab).removeClass("content-active");
      $('#link-desc').addClass("link-active");
      $("#description").addClass("content-active");
      active_link="#link-desc";
      active_tab="#description";
   });
   $('#link-rules').on('click',function(){
    $(active_link).removeClass("link-active");
    $(active_tab).removeClass("content-active");
    $('#link-rules').addClass("link-active");
    $("#rules").addClass("content-active");
    active_link="#link-rules";
    active_tab="#rules";
   });
    $('#link-contact').on('click',function(){
      $(active_link).removeClass("link-active");
      $(active_tab).removeClass("content-active");
      $('#link-contact').addClass("link-active");
      $("#contact").addClass("content-active");
      active_link="#link-contact";
      active_tab="#contact";
   });

 /* openPage(pageName,elmnt,color) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].style.backgroundColor = "";
    }
    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
  }*/
}

  ngAfterViewInit() { 
  
  
  }

  hackreg() {
    window.open("https://forms.gle/kEvPiQx4vNHVSZFW6","_blank");
  }
}
