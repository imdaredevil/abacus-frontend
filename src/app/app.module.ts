import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
//import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { EventsComponent } from './events/events.component';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { ProfileComponent } from './profile/profile.component';
import { WorkshopsComponent } from './workshops/workshops.component';
import { DescriptionComponent } from './description/description.component';
import { MessageComponent } from './message/message.component';
import { HomeComponent } from './home/home.component';
import { SponsorComponent } from './sponsor/sponsor.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { WorkdescComponent } from './workdesc/workdesc.component';
import { NonTechEventsComponent } from './non-tech-events/non-tech-events.component';
import { NonTechDescriptionComponent } from './non-tech-description/non-tech-description.component';
import { LoaderComponent } from './loader/loader.component';
import { WorkregComponent } from './workreg/workreg.component';
import { PaymentStatusComponent } from './payment-status/payment-status.component';
import { HackComponent } from './hack/hack.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'events', component: EventsComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'login', component: ProfileComponent },
  { path: 'workshops', component: WorkshopsComponent },
  { path: 'sponsors', component: SponsorComponent},
  { path: 'description', component: DescriptionComponent},
  { path: 'changepass', component: ChangepasswordComponent},
  { path: 'forPass', component: ForgotpasswordComponent },
  { path: 'non-tech', component: NonTechEventsComponent },
  { path: 'workreg/:id/:name', component: WorkregComponent},
  { path: 'paymentStatus/:id', component: PaymentStatusComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    EventsComponent,
    AboutUsComponent,
    ProfileComponent,
    WorkshopsComponent,
    DescriptionComponent,
    MessageComponent,
    HomeComponent,
    SponsorComponent,
    ChangepasswordComponent,
    ForgotpasswordComponent,
    WorkdescComponent,
    NonTechEventsComponent,
    NonTechDescriptionComponent,
    LoaderComponent,
    WorkregComponent,
    PaymentStatusComponent,
    HackComponent,
  ],
  imports: [
   // AngularFontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false,passThruUnknownUrl: true }
    ),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } 
    ),
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
