import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { FormGroup, FormControl } from '@angular/forms';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {
  logForm = new FormGroup({
    email : new FormControl(''),
  })

  enabled : string
   constructor(private profileservice: ProfileService, private messageservice : MessageService) { 
  }
  ngOnInit(): void {
    this.enabled = "no-loader";
      this.messageservice.clear();
  }

  validateEmail(email: string){
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
  {
    return true;
  }
    return false;
  }

  onSubmitLog() {
    var formvalues = this.logForm.value;
    var email = formvalues["email"];
    if(!this.validateEmail(email))
      this.messageservice.add("enter proper email");
    else
    {
      this.enabled = "loader";
      $(".formContent").css("display","none");
      this.profileservice.forpass(email).subscribe(resp => {
        this.enabled = "no-loader";
        $(".formContent").css("display","block");
        this.logForm.reset();
        this.messageservice.add(resp["message"]);
      });
    }
    
  }


}
