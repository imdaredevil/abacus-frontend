import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Test } from './test';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const tests = [
      { id: 11, desc: 'Dr Nice' },
      { id: 12, desc: 'Narco' },
      { id: 13, desc: 'Bombasto' },
      { id: 14, desc: 'Celeritas' },
      { id: 15, desc: 'Magneta' },
      { id: 16, desc: 'RubberMan' },
      { id: 17, desc: 'Dynama' },
      { id: 18, desc: 'Dr IQ' },
      { id: 19, desc: 'Magma' },
      { id: 20, desc: 'Tornado' }
    ];
    return {tests};
  }

  genId(heroes: Test[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}