export const events_list = [
    {
        date: '7', 
        month: "Mar", 
        name: "Programming Loopholes",
        short_desc: "This iconic event checks ,how deep participants can penetrate into a system by concentrating on time and ...",
        detail_desc: "This iconic event checks, how deep participants can penetrate into a system by concentrating on time and space complexities.<br/>"+
        "<b>Prelims:</b><br/>Written round which requires logic based creativity to solve tricky and hidden concepts in known topics.This event mainly focuses on optimisation of a code.<br/>" + 
         "<b>Finals:</b><br/> This round is based on data structures and algorithm.<br>"+
         "<center><br><b>Date: March 7</b></center>"+
        "<center><b>Venue: KP 102,103</b><br></center>"+
        "<center><b>Time: 9:00am to 11:30am</b></center>"+
        "<center><br/><br/><h3>Participate and win placement opportunities at <b>NIAGARA NETWORKS</b></h3></center>",
        image: "./assets/images/events/niagara1.png",
        rules: "<ul><li>A team should contain exactly two members.</li>"+
        "<li>A participant cannot be a member of more than one team.</li>"+
        "<li>The participants are not allowed to bring any additional material</li>"+
        "<li>Teams involved in any kind of malpractice will be disqualified immediately</li>"+
        "<li>Marks will be provided based on the efficiency of the solution</li>"+
        "<li>Decisions made by the administrators will be final.</li>",
        contact:"<ul><li><b>Abinayaa S K C</b> : +91 8300751369</li><li><b>Sri Harini K</b> : +91 9080495091</li><li><b>Guru Shruthy G</b> : +91 7826909111</li></ul>"
    },
    {
        date: "6",
        month: "Mar",
        name:"Code for Queens",
        short_desc:"A women only event meant to close the gender gap in the coding community. A great platform where every girl ...",
        detail_desc:"A women only event meant to close the gender gap in the coding community. A great platform where every girl unleashes the tech-gigged geek Goddesses within them by their logical thinking and creativity.<br>"+
        "<center><strong><upper><i>“Disentangle the script and confront your coding art”</i></upper></strong></center>"+
        "<b>Prelims: </b><br/> Pen and paper Round which consists of aptitude (both tech and non-tech).Technical questions will be based on Data structures.Duration is 40 mins.<br/>"+
        "<b>Round 2:</b><br/> Technical Quiz on Data Structures and Algorithms.Duration is 20 mins.<br/>"+
        "<b>Round 3:</b><br/>Coding contest.The first person will be allowed to code for the first 15 minutes,next 15 minutes the next person and the next 15 minutes both of them can code.<br/>"+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: KP 205,206</b><br></center>"+
        "<center><b>Time: 10:00am to 1:00pm</b></center>"+
        "<center><br/><br/><h3>Participate and win internship opportunities at <b>AMAZON</b></h3></center>",
        image: "./assets/images/events/amazon.png",
        rules:"<ul><li>A team should consist of two members.</li>"+
        "<li>Teams involved in any kind of malpractice will be disqualified immediately</li>"+
        "<li>Decisions made by the administrators will be final</li>"+
        "<li>During code relay,there should not be any kind of communication between the 2 participants.</li><ul>",
        contact:"<ul><li><b>Divya S :</b> 9597387511</li>"+
        "<li><b>Mohana Ruba R :</b> 8220071919</li>"+
        "<li><b>Srinithi B :</b> 7448850772</li></ul>",
    },
    {
        date: "7",
        month: "Mar",
        name:"Deadlock Opener",
        short_desc:"This event challenges the complex database query solving skills of programmers testing for injections and deadlock ...",
        detail_desc:"This event challenges the complex database query solving skills of programmers testing for injections and deadlock situations. This event mainly focuses on the critical sections of database. This finds way to improve interface between database and users<br/>"+
        "<b>Prelims:</b><br/>A pen and paper round consisting of MCQs, query solving and questions from well known concepts of database within a duration of 45 minutes.<br/>"+
         "<b>Finals:</b><br/>Two tasks will be given which involves logic based creativity to solve few challenging thinking out of the box problems.<br/>"+
         "<center><br><b>Date: March 7</b></center>"+
        "<center><b>Venue: KP 205,206</b><br></center>"+
        "<center><b>Time: 9:30am to 12:30pm</b></center>",
        image: "./assets/images/events/icon3.png",
        rules: "<ul><li>A team should have two members</li>"+
        "<li>The participants are not allowed to bring any additional material</li>"+
        "<li>Teams involved in any kind of malpractice will be disqualified immediately</li>"+
        "<li>Decisions made by the administrators will be final</li>"+
        "<li>Marks will be provided based on the efficiency of the solution</li></ul>",
        contact:"<ul><li><b>Varsha : </b> 9489463257</br></li>"+
        "<li><b>Kavya :</b> 8903748043</li>"+
        "<li><b>Harshini :</b> 9500699015</li></ul>"
    },
    {
        date: "6",
        month: "Mar",
        name:"Crypt your mind",
        short_desc:"Ever wanted to break the enigma? Can you encrypt the stories and decrypt your path to success? Do you want to delve ...",
        detail_desc:"Ever wanted to break the enigma? Can you encrypt the stories and decrypt your path to success? Do you want to delve into a plethora of challenges? Then Crypt Your Mind is the perfect place for you to be! <br/>"+
        "<b>Prelims </b><br/>Participants will have 45 minutes to navigate their way through quizzes, puzzle solving and programming. The questions will be based on ciphers, cryptography and cybersecurity<br/> "+
        "<b>Finals </b><br/>The finals will be a series of 3 rounds. Each round will be conducted in a sudden death format. The team that completes all the rounds first will be the winner"+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: KP 102,103</b><br></center>"+
        "<center><b>Time: 1:00pm to 4:00pm</b></center>",
        image: "./assets/images/events/code.png",
        rules: "<ul><li>Teams can consist of a maximum of 3 members</li>"+
        "<li>We don’t accept lone wolves.</li>"+
        "<li>The top performing teams in the prelims will qualify for the finals.</li>",
        contact:"<ul><li><b>Ishwarya R:</b> 8610279253</li>"+
        "<li><b>Nishaali M :</b> 9123544519</li>"+
        "<li><b>Ishwarya S :</b>   7358234738</li></ul>"
    },
    {
        date: "6",
        month: "Mar",
        name:"Datathon",
        short_desc:"The event tests the analytical and critical skills of participants, which are the two main prerequisites for anyone ...",
        detail_desc:"The event tests the analytical and critical skills of participants, which are the two main prerequisites for anyone interested in Data Science and analytics. Given a problem statement and scenario, the team which can present a solution covering all dimensionality with consideration of the given constraints will emerge victorious in the end.<br/>"+
        "This event will have  quiz based on machine learning and data science concepts.<br/>"+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: KP 207</b><br></center>"+
        "<center><b>Time: 9:00am to 11:30am</b></center>"+
        "<center><br/><br/><h3>Participate and win internship opportunities at <b>ZUCI SYSTEMS</b></h3></center>",
        image: "./assets/images/events/zuci.png",
        rules: "<ul><li>A team must consist of 2 members</li>"+
        "<li>The team members need not necessarily be from the same institution/college</li>"+
        "<li>Participants can not be a member of more than a team</li>"+
        "<li>Teams must remain the same throughout the event</li>"+
        "<li>Teams involved in any kind of malpractice will be disqualified immediately</li>"+
        "<li>The decision of the judges will be final</li></ul>",
        contact:"<ul><li><b>Jessica :</b> 95666087849</li>"+
        "<li><b>Priyadharshini :</b> 8825477697</li>"+
        "<li><b>Kiruthya R :</b> 8610620938</li></ul>"
    },
    {
        date: "6",
        month: "Mar",
        name:"Problem-free Statement",
        short_desc:"This event provides all the coding techniques and concepts like reverse coding, debugging, code obfuscating to the ...",
        detail_desc:"This event provides all the coding techniques and concepts like reverse coding, debugging, code obfuscating to the programmers testing their might."+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: KP 102,103</b><br></center>"+
        "<center><b>Time: 9:00am to 11:30am</b></center>"+
        "<center><br/><br/><h3>Participate and win internship opportunities at <b>FRESHWORKS</b> and <br>placement opportunities at <b>NIAGARA NETWORKS</b></h3></center>",
        image: "./assets/images/events/freshworks1.png",
        rules: "<ul><li>A team should have two members.</li>"+
        "<li>Teams involved in any kind of malpractice will be disqualified immediately.</li>"+
        "<li>Decisions made by the administrators will be final.</li></ul>",
        contact:"<ul><li><b>Amuru manasa :</b> 9080204587</li>"+
        "<li><b>Srisha S :</b> 8940584057</li>"+
        "<li><b>Kota anusha :</b> 9490897180</li></ul>"
    },
    {
        date: "6",
        month: "Mar",
        name:"Packeting Socket",
        short_desc:"Are you someone  who is interested in networking?Do the terms protocols,ports and packets make you feel electric? ...",
        detail_desc:"Are you someone  who is interested in networking?Do the terms protocols,ports and packets make you feel electric?Always excited to guess how things are coordinated for your live TV shows?Then this is the right place to show your talents in solving network puzzles and programming problems."+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: KP 401,402</b><br></center>"+
        "<center><b>Time: 9:00am to 12:30pm</b></center>"+
        "<center><br/><br/><h3>Participate and win placement opportunities at <b>NIAGARA NETWORKS</b></h3></center>",
        image: "./assets/images/events/niagara1.png",
        rules:"<ul><li>Max of two per team</li>"+
        "<li>Teams must remain the same throughout the event</li>"+
        "<li>Teams involved in any kind of malpractice will be disqualified immediately.</li>"+
        "<li>The decision of the judges will be final</li><ul>",
        contact:"<ul><li><b>Arshadh :</b> 8148775755 </li>"+
        "<li><b>Hasna :</b> 8489976488</li>"+
        "<li><b>Harini  :</b>  9787402668 </li></ul>"
    },
    {
        date: "6",
        month: "Mar",
        name:"OSPC",
        short_desc:"An arena of competitive programming, ‘OSPC’ provides a real-time environment where coders compete on the spot ...",
        detail_desc:"An arena of competitive programming, ‘OSPC’ provides a real-time environment where coders compete on the spot with their likes. This allows the programmers to hone their programming skills and benchmark themselves against the other competent programmers<br/>"+
        "It is an event to nourish your problem solving skills.On-Site Programming Contest (OSPC) similar to ACM ICPC style of coding. Work to solve the most real-world problems, fostering collaboration, creativity, innovation, and the ability to perform under pressure."+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: RUSA Gallery</b><br></center>"+
        "<center><b>Time: 9:30am to 12:00pm</b></center>"+
        "<br/><br/><center><h3>Participate and win internship and placement opportunities at <b>PandoCorp</b></h3></center>",
        image: "./assets/images/events/pando.png",
        rules:"<ul><li>This is an individual event. All lonewolves are welcome.</li>"+
        "<li>Participants from different colleges are allowed</li>"+
        "<li>Coding platform : a compeititve programming round in online platform</li>"+
        "<li>Duration of 45 mins for the first round and 90 minutes for the second round</li>"+
        "<li>Any sort of malpractice can lead to disqualification of team</li></ul>",
        contact:"<ul><li><b>Ashvathe :</b> 9442799731</li>"+
        "<li><b>Harshini :</b> 8248188019</li>"+
        "<li><b>Karthikeyan :</b> 9176581000</li></ul>"
    },
    {
        date: "7",
        month: "Mar",
        name:"Booting Novice",
        short_desc:"For all the blooming programmers out there!This is an exclusive event for you.programming today is a ...",
        detail_desc:"For all the blooming programmers out there!This is an exclusive event for you.programming today is a race between software engineers striving to build bigger and better programs.if you are a blooming bud in programming,then this is a great opportunity to start with.an event which touches the basics of every nook of computer science<br/>"+
        "This event  helps freshers laying the firm basement introducing them to the coding world .This mines and finds programmers among freshers with their subjective practical knowledge and logical skills.</br>"+
        "<b><center><i>“IT ALL BEGINS HERE”</i></center></b>"+
        "<center><br><b>Date: March 7</b></center>"+
        "<center><b>Venue: Ground floor lab</b><br></center>"+
        "<center><b>Time: 9:00am to 11:30am</b></center>",
        image: "./assets/images/events/icon3.png",
        rules:"<ul><li>Only first year can participate(bring your ID cards)</li>"+
        "<li>Two members per team</li>"+
        "<li> Lone wolves aren’t entertained</li><ul>",
        contact:"<ul><li><b>Vinoth  :</b> 9629160571</li>"+
        "<li><b>Udhaya prasath :</b> 9677114537</li>"+
        "<li><b>Yuvaraja  :</b> 8610183128</li></ul>",
    },
    {
        date: "6",
        month: "Mar",
        name:"Codestorm",
        short_desc:"This event test participants adept in obfuscating the code and optimizing the solution for the program. This ...",
        detail_desc:" This event test participants adept in obfuscating the code and optimizing the solution for the program.This checks the participants efficient approach on complex problems and advisability techniques. A participant with a sound knowledge of Data Structures and a eye for problem solving can excel at Codestrom.<br/>"+
        "<b>Prelims :</b><br/>"+
        "A Written round consists of questions based on Data structures ,Algorithms, logical reasoning and programming logic with a duration of 35 mins.<br/>"+
        "<b>Finals :</b><br/>"+
        "This round checks your ability to code in an annoying environment and your ability to code from a point where someone else has paused."+
        "<center><br><b>Date: March 6</b></center>"+
        "<center><b>Venue: KP 205,206</b><br></center>"+
        "<center><b>Time: 1:00pm to 4:00pm</b></center>"+
        "<center><br/><br/><h3>Participate and win internship opportunities at <b>ACCOLITE</b></h3></center>",
        image: "./assets/images/events/accolite.png",
        rules:"<ul><li>A team can have a minimum of one member and maximum of two members.</li>"+
        "<li>The team members need not necessarily be from same institution/college.</li>"+
        "<li>A participant cannot be a member of more than one team.</li>"+
        "<li>The participants are not allowed to bring any additional material.</li>"+
        "<li>Teams involved in any kind of malpractice will be disqualified immediately.</li>"+
        "<li>Decisions made by the administrators will be final.</li>"+
        "<li>Marks will be provided based on the efficiency of the solution.</li><ul>",
        contact:"<ul><li><b>Vijayakumar :</b> +91 8300424892</li>"+
        "<li><b>Srinath :</b> +91 9486289134</li>"+
        "<li><b>Mouli Prakash :</b> +91	8142889937</li></ul>",
    },
    {
        date: "7",
        month: "Mar",
        name:"Web doodler",
        short_desc:"To the ones having an eye for both aesthetics and functionality and can convert a vision into a stunning website ...",
        detail_desc:"To the ones having an eye for both aesthetics and functionality and can convert a vision into a stunning website,web doodles is here ..answer a few simple questions in the first round and show your website building prowess in the second.grab exciting prizes and put your building skills to test.<br/>"+
        "This event focuses on programmers’ skills in web scripting and designing making them explore flexible, malware-free, hacker-free web application development. This demands theoretical knowledge as well as creative exploration<br/>"+
        "<center><b><i>“VISUALIZE.SHAPE.CODE.CREATE”</i></b></center>"+
        "<center><br><b>Date: March 7</b></center>"+
        "<center><b>Venue: KP 401,402</b><br></center>"+
        "<center><b>Time: 9:30am to 12:30pm</b></center>"+
        "<center><br/><br/><h3>Participate and win internship opportunities at <b>Cyces Innovation labs</b></h3></center>",
        image: "./assets/images/events/cyces.png",
        rules: "<ul><li>A team must consist of 2 members</li>"+
        "<li>The team members need not necessarily be from the same institution/college</li>"+
        "<li>Participants can not be a member of more than a team</li>"+
        "<li>Teams must remain the same throughout the event</li>"+
        "<li>Teams involved in any kind of malpractice will be disqualified immediately</li>"+
        "<li>The decision of the judges will be final</li><ul>",
        contact:"<ul><li><b> Aravindan :</b> 9786752336</li>"+
        "<li><b>Gowsalya :</b> 8438498725</li>"+
        "<li><b>Shalini  :</b> 7708779704</li>"+
        "<li><b>Deepthika s :</b> 8056754752</li></ul>",
    },
    {
        date: "2",
        month: "Mar",
        name:"OLPC",
        short_desc:"This is a online programming contest where we tests the competitive programmers ability by non-trivial usage of ... ",
        detail_desc:"This is a online programming contest where we tests the competitive"+
        "programmers ability by non-trivial usage of a well known programming concepts.It is designed to be an holistic event that will test the participant’s knowledge of Algorithms, Data Structures and also have competitive coding as one of its primary components.<br/>"+
        "<center><br><b>Date: March 2</b></center>"+
        "<center><b>Time: 9:00am to 11:30pm</b></center>",
        image: "./assets/images/events/icon3.png",
        rules:"<ul><li>Those who involve in any kind of malpractice will be disqualified immediately.</li>"+
        "<li>The decision of the judges will be final.</li></ul>",
        contact:"<ul><li><b>Prethiga devi :</b> 9047800493</li>"+
        "<li><b>Sai sundhar :</b> 9677947784</li></ul>"
    },
    {
        date: "6",
        month: "Mar",
        name:"Coding Hungama",
        short_desc:"",
        image: "../../assets/images/events/amazon.png",
        detail_desc:"<div class=\"center\"><p>Come let's</p><h1>Code All Night</h1><p>with</p><img src=\"../../assets/images/events/amazon.png\"><br><br><p>Here comes yet another all night event of Abacus for the coders in you. Express your data structures and algorithmic ability and solve problems all night to show that you are the best. Be there on March 4th for this coding marathon to win prizes worth <b>&#8377; 10000</b> and internship opportunites from <b>Amazon</b>. So, Get.. Set.. Hit Register..</p><b>Date: March 4th</b><br><b>Time: 8 PM to 8 AM</b><br><b>Venue: CSE Department, CEG</b></div>",
        rules:"<ul><li>Each team should consist of two members.</li>"+
        "<li>There will be multiple rounds with breaks in-between.</li>"+
        "<li>The team members can be from different colleges and departments.</li>"+
        "<li>The judges decision is final.</li><ul>",
        contact:"<b>Inbaraj :</b> +91 96004 39693 <br/>"+
        "<b>Visak :</b> +91 84896 57631 <br/>"
    }
];
