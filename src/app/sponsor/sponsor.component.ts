import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sponsor',
  templateUrl: './sponsor.component.html',
  styleUrls: ['./sponsor.component.css']
})
export class SponsorComponent implements OnInit {

  title : any
  tshirt: any
  hackathon: any
  placement: any
  events: any[]
  workshops: any[]

  constructor() { 
   this.title={"src": './assets/images/sponsors/motorq.png',"id":"motorq"};
   this.tshirt={"src": './assets/images/sponsors/pando.png',"id":"pando"};
   this.hackathon={"src": './assets/images/sponsors/chronus.png',"id":"chronus"};
   this.placement={"src": './assets/images/sponsors/niagara.png',"id":"niagara"};
   this.events = [];
   this.events.push({"src": './assets/images/sponsors/accolite.png',"id":"accolite"});
   this.events.push({"src": './assets/images/sponsors/amazon.png',"id":"amazon"});
   this.events.push({"src": './assets/images/sponsors/zuci.png',"id":"zuci"});
   this.events.push({"src": './assets/images/sponsors/cyces.png',"id":"cyces"});
   this.events.push({"src": './assets/images/sponsors/freshworks.png',"id":"freshworks"});
   this.workshops = [];
   this.workshops.push({"src": './assets/images/workshops/amazon.png',"id":"amazon1"});
   this.workshops.push({"src": './assets/images/workshops/makerstribe.png',"id":"makerstribe"});
   this.workshops.push({"src": './assets/images/workshops/fourkites.png',"id":"fourkites"});
   this.workshops.push({"src": './assets/images/workshops/IBM.png',"id":"ibm"});
   /*this.workshops.push({"src": './assets/images/workshops/visualbi.png',"id":"amazon"});*/
  }
  ngOnInit() {
  }

}
