export const workshops_list = [
    {
        date: '7', 
        month: "Mar", 
        name: "Data Structures and Algorithms",
        company_logo: "./assets/images/workshops/amazon.png",
        short_desc: "By",
        detail_desc: "Every expertise is built upon a strong foundation. Here is an opportunity for you to get guidance from the experts on how to build a strong foundation on data structures and algorithms.<br>"+
        "<b>Registration Fee: </b>"+
        "&#8377; 500<br>"+
        "<b>Venue: </b>"+
        "RUSA Gallery<br>"+
        "<b>Date: </b>"+
        "March 7<br>"+
        "<b>Time: </b>"+
        "9:00am to 4:30pm<br>"+
        "<br/><h5><b>Sorry we are full. Check out our other workshops which are equally interesting and useful.</b></h5>",
        contact: "<ul><li><b>Mounika </b> : +91 94922 34952</li><li><b>Swarnamalya </b> : +91 97877 04044</li></ul>",
        comingsoon: false
    },
    {
        date: "6",
        month: "Mar",
        name:"Computer Vision  Workshop",
        company_logo: "./assets/images/workshops/visualbi.png",
        short_desc:"By",
        detail_desc:"No one would wonder if someone says computer have eyes today. This workshop is especially made for the image processing enthusiasts who have the knack to know how computers see, detect and process what they see.<br>"+
        "<b>Registration Fee: </b>"+
        "&#8377; 500<br>"+
        "<b>Venue: </b>"+
        "Ground floor lab<br>"+
        "<b>Date: </b>"+
        "March 6<br>"+
        "<b>Time: </b>"+
        "9.00am to 4.30pm<br>",
        contact: "<ul><li><b>Jothe </b> : +91 98406 17050</li><li><b>Priyavarshini </b> : +91 72993 88156</li></ul>",
        comingsoon: false
    },
    {
        date: "7",
        month: "Mar",
        name:"Blockchain Workshop",
        company_logo: "./assets/images/workshops/IBM.png",
        short_desc:"By",
        detail_desc:"Blockchain technology is considered as one of the most popular catchphrases in recent times.IBM Blockchain Platform is an enterprise-ready cloud service designed to accelerate the development, governance, and operation of a multi-institution network. Step inside and know how the blockchain technology works with experts from IBM!<br>"+
        "<b>Registration Fee: </b>"+
        "&#8377; 500<br>"+
        "<b>Venue: </b>"+
        "Second floor lab<br>"+
        "<b>Date: </b>"+
        "March 7<br>"+
        "<b>Time: </b>"+
        "9.00am to 4.30pm<br>" +
        "<br/><h5><b>Sorry we are full. Check out our other workshops which are equally interesting and useful.</b></h5>",
        contact: "<ul><li><b>Abitha </b> : +91 96778 11142</li><li><b>Naveena </b> : +91 90038 61652</li></ul>",
        comingsoon: false
    },
    {
        date: "5,6",
        month: "Mar",
        name:"Introduction to Data Science",
        company_logo: "./assets/images/workshops/fourkites.png",
        short_desc:"By",
        detail_desc:"Ask a dozen people what “data science” means, and you get back thirteen different answers. This vagueness is unfortunately accompanied with a lot of hype and misaligned expectations. In this workshop, the experts from Fourkites are here to mitigate this by taking a good look under the hood of data science<br>"+
        "<b>Registration Fee: </b>"+
        "&#8377; 500<br>"+
        "<b>Time: </b><br>"+
        "March 5 - 9.30am to 12.30pm<br>"+
        "March 6 - 9.30am to 12.30pm<br>" +
        "<br/><h5><b>Sorry we are full. Check out our other workshops which are equally interesting and useful.</b></h5>",
        contact: "<ul><li><b>Rakkesh </b> : +91 97108 22718</li><li><b>&nbsp;Thilak Aswin </b> : +91 99597 20255</li></ul>",
        comingsoon: false
    },
    {
        date: "6",
        month: "Mar",
        name:"First step towards IIOT",
        company_logo: "./assets/images/workshops/makerstribe.png",
        short_desc:"By",
        detail_desc:"Being the starting point to take your career in Industrial IOT, this workshop deals with the understanding of the big picture of the industrial IoT and how to connect the dots for the better IoT products/services. The experts from Maker's Tribe are here to give you a deep understanding of communication protocols and the entire IoT architecture. Register and get ready to build the best IoT solution for a particular problem.<br>"+
        "<b>Registration Fee: </b>"+
        "&#8377; 500<br>"+
        "<b>Venue: </b>"+
        "Third floor lab<br>"+
        "<b>Date: </b>"+
        "March 6<br>"+
        "<b>Time: </b>"+
        "1.00pm to 5.00pm<br>",
        contact: "<ul><li><b>Geetha Lakshmi </b> : +91 82481 54092</li><li><b>Serlin Ester </b> : +91 99522 70749</li><li><b>Maheswari </b> : +91 84285 58285</li></ul>",
        comingsoon: false
    },
    
];
