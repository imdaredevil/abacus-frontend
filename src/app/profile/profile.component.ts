import { Component, OnInit, Input } from '@angular/core';
import { MessageService } from '../message.service';
import { ProfileService } from '../profile.service';
import { FormGroup, FormControl } from '@angular/forms';
import  { Router } from '@angular/router';
import { years } from './years';
import { departments } from './department';
import { colleges } from './colleges';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {


  regForm = new FormGroup({
    name : new FormControl(''),
    college: new FormControl(''),
    year: new FormControl(''),
    phone: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
    cpassword: new FormControl(''),
    department: new FormControl(''),
    state: new FormControl(''),
    city: new FormControl('')
  });

  logForm = new FormGroup({
    email : new FormControl(''),
    password: new FormControl('')
  })

  enabled : string;

  @Input() user: any
  years = years;
  departments = departments;
  colleges = colleges;

  constructor(private messageservice : MessageService, public profileservice : ProfileService, private router: Router) { 
    this.user = {
      "name" : "",
      "code"  : "",
    }
    this.enabled = "no-loader";
    var cookies = {};
            var cookieString = document.cookie;
            var ca = cookieString.split(";");
            for(var i=0;i<ca.length;i++)
            {
                var cookie = ca[i];
                var keyval = cookie.split("=");
                var key = keyval[0];
                var value = keyval[1];
                cookies[key] = value;
            }
           //console.log(cookies);
            this.user.code = cookies[" user-code"];
            this.user.name = cookies[" user-name"];
            this.user.registered = (cookies[" user-paid"] == "true");
            this.user.referrals = cookies[" referrals"];
            

  }

  loggedIn() {
    var cookies = {};
    var cookieString = document.cookie;
            var ca = cookieString.split(";");
            for(var i=0;i<ca.length;i++)
            {
                var cookie = ca[i];
                var keyval = cookie.split("=");
                var key = keyval[0];
                var value = keyval[1];
                cookies[key] = value;
            }
            if(cookies["user-token"])
                return true;
            return false;
  }

  ngOnInit() {

    var cookies = {};
            var cookieString = document.cookie;
            var ca = cookieString.split(";");
            for(var i=0;i<ca.length;i++)
            {
                var cookie = ca[i];
                var keyval = cookie.split("=");
                var key = keyval[0];
                var value = keyval[1];
                cookies[key] = value;
            }
          // console.log(cookies);
            this.user.code = cookies[" user-code"];
            this.user.name = cookies[" user-name"];
            this.user.registered = (cookies[" user-paid"] == "true");
            this.user.referrals =  cookies[" referrals"];
            if(this.loggedIn())
          {
            this.enabled = "loader";
            $(".formContent").css("display","none");
            this.profileservice.getpayments(cookies["user-token"]).subscribe(resp => {
              $(".formContent").css("display","block");
              this.enabled = "no-loader";
              if(resp["result-code"] == 200)
              this.user.payments = resp["payments"];
              else
                this.messageservice.add(resp["message"]);
            })
          }

  }

  validateEmail(email: string){
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
  {
    return true;
  }
    return false;
  }

  validatePhone(phone: string){
    var phoneno = /^\d{10}$/;
  if(phone.match(phoneno))
        {
          return true;
        }
      else
        {
          return false;
        }
  }
  onSubmitLog() {
    this.user = {
      "name" : "",
      "code" : ""
    }
    var formvalues = this.logForm.value;
    if(formvalues["email"] == "")
      this.messageservice.add("Enter email");
    else if(formvalues["password"] == "")
      this.messageservice.add("Enter password");
    else if(!this.validateEmail(formvalues["email"]))
      this.messageservice.add("Enter proper mail");
    else
    {
     // console.log(formvalues);
     this.enabled = "loader";
     $(".formContent").css("display","none");
        this.profileservice.login(formvalues).subscribe(
          response => {
            console.log(response);
            this.logForm.reset();
            $(".formContent").css("display","block");
            this.enabled = "no-loader";
            if(response["result-code"] == 200)
            {
              document.cookie = "user-token=" + response["userToken"];     
              document.cookie = "user-code=" + response["userCode"];
              document.cookie = "user-name=" + response["userName"];
              document.cookie = "user-paid=" + response["registered"];
              document.cookie = "referrals=" + response["referrals"];
              this.user.code = response["userCode"];
              this.user.name = response["userName"];
              this.user.registered = response["registered"];
              this.user.referrals = response["referrals"];
              this.ngOnInit();
            }
            else
            {
              this.messageservice.add(response["message"]);
            }
          });

    }
    
  }
  onSubmitReg() {
    this.messageservice.clear();
    var formvalues = this.regForm.value;
    if(formvalues["name"] == "")
      this.messageservice.add("Enter name");
    else if(formvalues["phone"] == "")
      this.messageservice.add("Enter phone");
    else if(formvalues["year"] == "")
      this.messageservice.add("Enter year");
    else if(formvalues["department"] == "")
      this.messageservice.add("Enter department");
    else if(formvalues["college"] == "")
      this.messageservice.add("Enter college");
    else if(formvalues["email"] == "")
      this.messageservice.add("Enter email");
    else if(formvalues["state"] == "")
      this.messageservice.add("Enter state");
    else if(formvalues["city"] == "")
      this.messageservice.add("Enter city");
    else if(formvalues["password"] == "")
      this.messageservice.add("Enter password");
    else if(formvalues["cpassword"] == "")
      this.messageservice.add("Confirm your password");
    else if(!this.validateEmail(formvalues["email"]))
      this.messageservice.add("Enter proper mail");
    else if(!this.validatePhone(formvalues["phone"]))
      this.messageservice.add("Enter proper phone");
    else if(formvalues["password"] != formvalues["cpassword"])
      this.messageservice.add("Password and Confirm password does not match");
    else
    {
      delete formvalues["cpassword"];
      this.enabled = "loader";
      $(".formContent").css("display","none");
        this.profileservice.signup(formvalues).subscribe(
          response => {
              //console.log(response);
              this.regForm.reset();
              $(".formContent").css("display","none");
              this.enabled = "no-loader";
              if(response["result-code"] == 200)
              {
                window.open("/login");
                this.messageservice.add("registration successful");
              }
              else
              {
                this.messageservice.add(response["message"]);
              }
          }
        );
    }     
  }

  logout() {
    document.cookie = "user-token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    document.cookie = "user-code= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    document.cookie = "user-name= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    document.cookie = "user-paid= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    document.cookie = "referrals= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
  }

  accomod() {
    window.open("https://forms.gle/gbVRLfqKf2DSSJgJ6","_blank");
  }
  eventreg(){
    var cookies = {};
            var cookieString = document.cookie;
            var ca = cookieString.split(";");
            for(var i=0;i<ca.length;i++)
            {
                var cookie = ca[i];
                var keyval = cookie.split("=");
                var key = keyval[0];
                var value = keyval[1];
                cookies[key] = value;
            }
            var formvalues = {};
            formvalues["token"] = cookies["user-token"];
            formvalues["referralCode"] = "none";
            formvalues["workshopName"] = "none";
            formvalues["workshopId"] = 0;
            this.enabled = "loader";
            $(".formContent").css("display","none");
      this.profileservice.register(formvalues).subscribe(resp => {
          this.logForm.reset();
          $(".formContent").css("display","block");
          this.enabled = "no-loader";
          if(resp["result-code"] == "200")
          {
              window.open(resp["redirect_url"]);
          }
          else
          {
            this.messageservice.add(resp["message"]);
          }
      });
  }
}
